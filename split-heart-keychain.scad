/*
* Split Heart Keychain (c) by Andrew A. Tasso
* 
* Split Heart Keychain is licensed under a Creative Commons 
* Attribution-NonCommercial-ShareAlike 4.0 International License.
* 
* You should have received a copy of the license along with this
* work. If not, see <http://creativecommons.org/licenses/by-nc-sa/4.0/>.
*/

// keep the number of fragments low while in preview to improve performance
$fn = $preview ? 20 : 100;

split_heart_keychain(size = 40, height = 8, left_letter="M", right_letter="A");

module split_heart_keychain(size, height, left_letter, right_letter) {

	left_half(size, height, left_letter);

	translate([5,0,0])
		right_half(size, height, right_letter);

}

module right_half(size, height, letter) {

	difference() {

		union() {

			heart(size = size, height = height);

			translate([(2/3) * size, (1/3) * size, height/2])
				rotate([0,0,25])
				translate([((1/3) * size) + (size / 20) ,0,0])
				ring(ring_diameter = size / 10, ring_thickness=size / 10);            

		}

		translate([(2/3) * size, (1/3) * size, height/2])
			linear_extrude(height = height/2 + 1)
			rotate([0,0,-45])
			text(text = letter, size = size/3, halign = "center", valign = "center");

		translate([0,0,-1])
			rotate([0,0,45])
			cube(size = [size * 2, size * 2, height + 2]);


	}

}

module left_half(size, height, letter) {

	difference() {

		union() {

			heart(size = size, height = height);

			translate([(1/3) * size, (2/3) * size, height/2])
				rotate([0,0,60])
				translate([((1/3) * size) + (size / 20) ,0,0])
				ring(ring_diameter = size / 10, ring_thickness=size / 10);      
		  
		}

		translate([(1/3) * size, (2/3) * size, height/2])
			linear_extrude(height = height/2 + 1)
			rotate([0,0,-45])
			text(text = letter, size = size/3, halign = "center", valign = "center");

		 translate([0,0,-1])
			rotate([0,0,-45])
			cube(size = [size * 2, size * 2, height + 2]);

	}

}

module heart(size, height) {

	difference() {

		union() {

			difference() {

				cube(size = [(2/3) * size,(2/3) * size,height]);

				//lower right side
				difference() {

					cube(size = [(2/3) * size, height/2, height]);

					translate([0,height/2,height/2])
						rotate([0,90,0])
						cylinder(d = height, h = (2/3) * size);

				}

				//lower left side
				difference() {

					cube(size = [height/2, (2/3) * size, height]);

					translate([height/2, 0,height/2])
						rotate([-90,0,0])
						cylinder(d = height, h = (2/3) * size);

				}

				//upper "notch"
				translate([((2/3) * size) - (height/2),((2/3) * size) - (height/2),0])
					cube(size = [height / 2, height / 2, height]);

			}

			//left side
			difference() {

				translate([(1/3) * size,(2/3) * size,0])
					cylinder(h = height, d = (2/3) * size);

				difference() {

					translate([(1/3) * size,(2/3) * size,0])
						rotate_extrude()
							translate([((1/3) * size) - height/2,0,-1])
							square(size = [height/2+1, height+1]);

					translate([(1/3) * size,(2/3) * size,height/2])
						ring(((1/3) * size)-(height/2), height);
						
				}

			}

			// right side
			difference() {

				translate([(2/3) * size,(1/3) * size,0])
					cylinder(h = height, d = (2/3) * size);


				difference() {

					translate([(2/3) * size,(1/3) * size,0])
						rotate_extrude()
							translate([((1/3) * size) - height/2,0,-1])
							square(size = [height/2+1, height+1]);

					translate([(2/3) * size,(1/3) * size,height/2])
						ring(((1/3) * size)-(height/2), height);
						
				}

			}

		}

	}
}

module ring(ring_diameter, ring_thickness) {
	
	rotate_extrude()
		translate([ring_diameter,0,0])
		circle(d = ring_thickness);

}